import driver.Driver;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.openqa.selenium.WebDriver;
import page.*;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class main {
    private WebDriver driver;
    public main() {
        Driver.setUp();
        this.driver = Driver.getDriver();
    }

    public void teste(String email) throws InterruptedException {
        HomeScreen screen = new HomeScreen(driver);
        screen.digitaEmail(email);
        screen.clicarPwned();
        Thread.sleep(5000);
        printarComVazamento(screen, email);
        printarSemVazamento(screen, email);

    }

    private void printarSemVazamento(HomeScreen screen, String email) {

        if(screen.obterStatusSucesso().equals("Good news — no pwnage found!")) {
            try {
                BufferedWriter write = Files.newBufferedWriter(Paths.get("HaveYouBeenPwned.txt"),
                        StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                CSVPrinter printer = new CSVPrinter(write, CSVFormat.EXCEL.withDelimiter('\t'));

                printer.printRecords(
                        String.format("E-mail testado: %s", email),
                        String.format("Parabéns! Seu e-mail nunca foi vazado!", screen.obterStatusSucesso())
                );
                printer.close(true);
            } catch (IOException e) {
            }
        }
    }

    private void printarComVazamento(HomeScreen screen, String email) {

        if(screen.obterStatus().equals("Oh no — pwned!")) {
            try {
                BufferedWriter write = Files.newBufferedWriter(Paths.get("HaveYouBeenPwned.txt"),
                        StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                CSVPrinter printer = new CSVPrinter(write, CSVFormat.EXCEL.withDelimiter('\t'));

                printer.printRecords(
                        String.format("E-mail testado: %s", email),
                        "Fui vazado? Infelizmente sim.",
                        String.format("Onde fui vazado? %s", screen.obterOnde()),
                        String.format("Data do vazamento? %s", screen.obterData()),
                        String.format("Mais informações: %s", screen.obterDetalhes())
                );
                printer.close(true);
            }
            catch (IOException e) {
            }
        }
    }
}
