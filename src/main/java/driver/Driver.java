package driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Driver {
    private static WebDriver driver;


    public static void setUp() {
        driver = abreNavegador();
    }

    public static WebDriver abreNavegador() {
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(false);
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\guih_\\chromedriver.exe");
        WebDriver driver = new ChromeDriver(options);
        // driver.manage().window().maximize();
        driver.get("https://haveibeenpwned.com/");
        return driver;

    }

    public static WebDriver getDriver() {
        return driver;
    }
}