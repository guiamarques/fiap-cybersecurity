package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class HomeScreen {
    private WebDriver driver;

    public HomeScreen (WebDriver driver){
        this.driver = driver;
    }

    public void digitaEmail(String email) throws InterruptedException {
        driver.findElement(By.id("Account")).sendKeys(email);
    }
    public void clicarPwned(){
        driver.findElement(By.id("searchPwnage")).click();
    }
    public String obterStatus(){
        return driver.findElement(By.xpath("//*[@id=\"pwnedWebsiteBanner\"]/div/div/div[1]/h2")).getText();
    }

    public String obterDetalhes(){
        String detalhes = driver.findElement(By.xpath("//*[@id=\"pwnedSites\"]/div/div/div/div/p")).getText();
        Pattern pattern = Pattern.compile("(?<=\\..?).+(?=)");
        Matcher matcher = pattern.matcher(detalhes);

        if(matcher.find()){
            return matcher.group();
        }
        return "Não há detalhe confirmado.";
    }

    public String obterOnde(){
        return driver.findElement(By.className("pwnedCompanyTitle")).getText();
    }
    public String obterStatusSucesso(){
        return driver.findElement(By.cssSelector("#noPwnage > div > div > div.pwnTitle > h2")).getText();
    }

    public String obterData(){
        String detalhes = driver.findElement(By.xpath("//*[@id=\"pwnedSites\"]/div/div/div/div/p")).getText();
        Pattern pattern = Pattern.compile("(?<=: In )(\\w+ \\d\\d\\d\\d)(?=, )");
        Matcher matcher = pattern.matcher(detalhes);

        if(matcher.find()){
            return matcher.group();
        }
        return "Não há data confirmada.";
    }

}
